from setuptools import setup

setup(
    name='altigen-extensions-gsheets',
    version='0.1',
    packages=[''],
    url='',
    license='',
    author='Phillip Stromberg',
    author_email='phillip@4stromberg.com',
    description='Syncs Altigen extensions with Google Sheets.',
    install_requires=[
        'altigen',
        'gspread',
        'oauth2client',
    ]
)
