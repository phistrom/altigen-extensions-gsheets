#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Creates or updates a spreadsheet on Google Sheets with up-to-date extensions.

Created by phillip on 10/3/2016
"""

from __future__ import print_function
import altigen
import argparse
import gspread
from oauth2client.service_account import ServiceAccountCredentials


# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
# SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SCOPES = ['https://spreadsheets.google.com/feeds']
SERVICE_ACCOUNT_KEY = 'service_account.json'
APPLICATION_NAME = 'Altigen Extension List Sync'


def main():
    parser = argparse.ArgumentParser("Altigen Extension Sync")
    parser.add_argument('altigen_database',
                        help=r"The path to your Altigen Internal.mdb file. This is usually "
                             r"located at C:\AltiDB\InternalDB\Internal.mdb on the phone system.")
    parser.add_argument('--name', default='Phone Extensions',
                        help="The name of the Google Workbook you want to open in your account. "
                             "Looks for a spreadsheet called 'Phone Extensions' by default. This spreadsheet must be "
                             "shared with the service account's email address "
                             "(i.e. credname@some-project.iam.gserviceaccount.com)")
    parser.add_argument('--id', help="An ID for an exact Google Sheet. Overrides the --name flag."
                                     "Spreadsheet must be shared with this service account!")
    parser.add_argument('--min-ext', default=100, type=int,
                        help="The lowest extension number to be put into the spreadsheet.")
    parser.add_argument('--max-ext', default=130, type=int,
                        help="The highest extension number to be put into the spreadsheet.")
    args = parser.parse_args()
    try:
        credentials = ServiceAccountCredentials.from_json_keyfile_name(SERVICE_ACCOUNT_KEY, SCOPES)
    except IOError:
        print("Please obtain a Service Account JSON key from Google's API dashboard, name it {keyname}, "
              "and place it in your working directory.".format(keyname=SERVICE_ACCOUNT_KEY))
        return -1
    gc = gspread.authorize(credentials)

    if args.id:
        wbk = gc.open_by_key(args.id)
    else:
        try:
            wbk = gc.open(args.name)
        except gspread.SpreadsheetNotFound:
            print("No spreadsheet named {name} was found. "
                  "Please specify a name with --name or create a '{name}' spreadsheet.".format(name=args.name))
            return -2

    altidb = altigen.open(args.altigen_database)
    extensions = altidb.get_extensions(args.min_ext, args.max_ext)
    extensions.sort(key=lambda x: x.first_name)
    num_extensions = len(extensions) + 1
    try:
        newsheet = wbk.worksheet('Extensions')
        if newsheet.row_count != num_extensions:
            newsheet.resize(num_extensions, max(newsheet.col_count, 3))
    except Exception as ex:
        print(ex)
        newsheet = wbk.add_worksheet(title='Extensions', rows=num_extensions, cols=3)
    cells = newsheet.range('A1:C%d' % num_extensions)
    rows = []
    for i in range(0, len(cells), 3):
        rows.append(cells[i:i + 3])
    rows[0][0].value, rows[0][1].value, rows[0][2].value = 'Number', 'First Name', 'Last Name'
    for idx, ext in enumerate(extensions, start=1):
        rows[idx][0].value = ext.number
        rows[idx][1].value = ext.first_name
        rows[idx][2].value = ext.last_name

    newsheet.update_cells(cells)


if __name__ == '__main__':
    exit(main())
